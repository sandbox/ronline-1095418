
Purchase counter module display number of purchases per product. 

Installation
------------

Copy the module to your sites/SITENAME/modules directory.
Go to the admin/build/modules and check the "Purchase Counter" check box. 

Author
------
Roman Hurtik  (http://drupal.org/user/555668)
